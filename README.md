# Kustomize-basic-sample

Ejemplo básico de utilización de kustomize.

Se trata de un despliegue de nginx que se realiza en un namespace llamado portales, que debemos crear previamente:

```bash
kubectl create namespace portales
```

Sobre él se pueden realizar dos despliegues, uno que simula un entorno de desarrollo, con un sólo pod y otro que simula un entorno de preproducción, con dos pods.

La descripción del proceso se encuentra en el artículo https://dvdcr.com/posts/2022/02/k8s-kustomize-intro/
